package com.gammatrace.integration;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.threeten.bp.Instant;
import org.threeten.bp.LocalDate;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZonedDateTime;

import com.gammatrace.datamodel.Trade;
import com.gammatrace.helpers.TestUtils;
import com.opengamma.analytics.financial.provider.calculator.discounting.ParRateDiscountingCalculator;
import com.opengamma.analytics.financial.provider.curve.CurveBuildingBlockBundle;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderDiscount;
import com.opengamma.gammatrace.instrument.Instrument;
import com.opengamma.gammatrace.instrument.InstrumentBuildManager;
import com.opengamma.gammatrace.instrument.SwapInstrument;
import com.opengamma.gammatrace.marketconstruction.curve.CurveMaker;
import com.opengamma.gammatrace.marketconstruction.dictionary.DictionaryManager;
import com.opengamma.gammatrace.rates.calculator.MarketStandardFairFixedRateCalculator;
import com.opengamma.util.tuple.Pair;

public class FixedONTradeTest {
	private static final ParRateDiscountingCalculator PRDC = ParRateDiscountingCalculator.getInstance();
	private static List<String> CURRENCIES = new ArrayList<>();
	static{
		CURRENCIES.add("USD");
		CURRENCIES.add("EUR");
		CURRENCIES.add("GBP");
		CURRENCIES.add("JPY");
	}
	
	@Test
	public void doReprice(){
		TestUtils parserUtils = new TestUtils();
		Set <Trade> trades = parserUtils.getRepriceableTrade();
		int counter = 0;
		
		ArrayList<String> results = new ArrayList<>();
		ArrayList<String> errors = new ArrayList<>();
		
		for (Trade trade : trades) {
			long id = trade.getDissemination_id();
			String taxonomy = trade.getTaxonomy();
			String currency1 = trade.getNotional_currency_1();
			String currency2 = trade.getNotional_currency_2();
			if (taxonomy.equalsIgnoreCase("InterestRate:IRSwap:OIS") && CURRENCIES.contains(currency1) && CURRENCIES.contains(currency2) && counter < 80) {	
				try{
					InstrumentBuildManager build = new InstrumentBuildManager(trade);
					Instrument instrument = build.makeInstrument();
					CurveMaker SCCM = DictionaryManager.makeDictionary(instrument);
					Pair<MulticurveProviderDiscount, CurveBuildingBlockBundle> pair = SCCM.makeCurve();
					MarketStandardFairFixedRateCalculator MSFFRC = new MarketStandardFairFixedRateCalculator((SwapInstrument) instrument, pair.getFirst());
					double pm1 = MSFFRC.getFairFixedRate();
					Pair<Double, Double> pm2 = MSFFRC.getTwoPartFairFixedRate();
					LocalDate effectiveDate = ZonedDateTime.ofInstant(Instant.ofEpochSecond(trade.getEffective_date()), ZoneId.of("UTC")).toLocalDate();
					LocalDate endDate = ZonedDateTime.ofInstant(Instant.ofEpochSecond(trade.getEnd_date()), ZoneId.of("UTC")).toLocalDate();
					if (pm2 != null) {
						results.add("ID: "+trade.getDissemination_id()+", effective: "+effectiveDate+", end: "+endDate+", additional: "+trade.getAdditional_price_notation()+
								" PM1: "+pm1*100+", PM2: "+pm2.getFirst()*100+", "+pm2.getSecond()*100);	
					} else {
						results.add("ID: "+trade.getDissemination_id()+", effective: "+effectiveDate+", end: "+endDate+", additional: "+trade.getAdditional_price_notation()+" PM1: "+pm1*100);
					}
					
				}
				catch (Exception e){
					System.out.println(trade.getDissemination_id() +":  "+e.getMessage());
					errors.add(e.toString());
					
				}
				counter++;
			}
		}
	
		System.out.println(counter);
		for (String s : results.toArray(new String[results.size()])) {
			System.out.println(s);
		}
		System.out.println(results.size());
		for (String s : errors.toArray(new String[errors.size()])) {
			System.out.println(s);
		}
	}
}
