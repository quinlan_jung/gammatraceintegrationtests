package com.gammatrace.integration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import com.gammatrace.commonutils.Utils;
import com.gammatrace.datamodel.Trade;
import com.gammatrace.helpers.FixedIborTradeSummary;
import com.gammatrace.helpers.TestUtils;
import com.gammatrace.logging.CustomLogger;
import com.opengamma.gammatrace.prod.RepriceResult;
import com.opengamma.gammatrace.prod.Repricer;
import com.opengamma.util.money.Currency;

public class FixedIborTradeTest {
	static{
		Utils.clearDirectory(CustomLogger.getLoggingPath());
	}
	Logger logger = CustomLogger.getLogger(FixedIborTradeTest.class);
	private static final TestUtils PARSER_UTILS = new TestUtils ();
	private static final Set<Trade> TRADES = PARSER_UTILS.getRepriceableTrade();
	private static final List<Long> EXCLUSION_LIST = new ArrayList<>();
	static {
		EXCLUSION_LIST.add(17092648L);
		EXCLUSION_LIST.add(17105845L);
		EXCLUSION_LIST.add(17104501L);
	}
	
	@Test
	public void doReprice(){
		HashMap<Long, FixedIborTradeSummary> tradeItems = new HashMap<>();
		List<Long> orderOfReprice = new ArrayList<>();
		List<RepriceResult> repriceResults = new ArrayList<>();
		int counter = 0;
		for (Trade trade : TRADES) {
			long id = trade.getDissemination_id();
			if (EXCLUSION_LIST.contains(id)) {
				continue;
			}
			Currency ccy = Currency.of(trade.getNotional_currency_1());
			if (trade.getTaxonomy().equalsIgnoreCase("InterestRate:IRSwap:FixedFloat") && counter < 100 && ccy.equals(Currency.EUR)) {
				FixedIborTradeSummary tradeSummary = new FixedIborTradeSummary(trade);
				if (tradeSummary.getEffectiveDate().equals(tradeSummary.getEndDate())) {
					continue;
				}
				orderOfReprice.add(id);
				tradeItems.put(id, tradeSummary);
				logger.info(String.format("Repricing dissemination id : %d", id));
				tradeSummary.startTimer();
				RepriceResult repriceResult = reprice(trade);
				tradeSummary.setFairFixedRate(repriceResult.getTradeImpliedFairQuote());
				tradeSummary.setMarketImpliedFixedRate(repriceResult.getMarketImpliedFairQuote());
				/*
				if (repriceResult != null){
					repriceResults.add(repriceResult);
				}*/
				//counter++;
			}
		}
	
		for (Long id : orderOfReprice){
			FixedIborTradeSummary tradeSummary = tradeItems.get(id);
			//logger.info(tradeSummary.getDisseminationId()+"  "+tradeSummary.getEffectiveDate()+"  "+tradeSummary.getPriceNotation()+"  "+tradeSummary.getFairFixedRate());
			tradeSummary.printResults();
		}
		
		//Assert.assertEquals(repriceResults.size(), tradeItems.size());
	}
	
	private RepriceResult reprice(Trade trade){
		try{
			return Repricer.reprice(trade);	
		}
		catch (Exception e){
			logger.info(trade.getSettlement_currency() +":  "+e.getMessage() + trade.getDissemination_id() );
			e.printStackTrace();
			return null;
		}
	}
}
