package com.gammatrace.integration;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;
import org.threeten.bp.Instant;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZonedDateTime;

import com.gammatrace.commonutils.Utils;
import com.gammatrace.datamodel.Trade;
import com.gammatrace.helpers.TestUtils;
import com.gammatrace.logging.CustomLogger;
import com.opengamma.analytics.financial.interestrate.swap.derivative.Swap;
import com.opengamma.analytics.financial.provider.calculator.discounting.ParSpreadMarketQuoteDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueDiscountingCalculator;
import com.opengamma.analytics.financial.provider.curve.CurveBuildingBlockBundle;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderDiscount;
import com.opengamma.gammatrace.rates.calculator.FairFixedRateCalculator;
import com.opengamma.gammatrace.rates.instrument.SwapFixedIborBuilder;
import com.opengamma.gammatrace.rates.instrument.SwapFixedONBuilder;
import com.opengamma.util.money.Currency;
import com.opengamma.util.money.MultipleCurrencyAmount;
import com.opengamma.util.time.DateUtils;
import com.opengamma.util.tuple.Pair;

public class TimeSeriesTest {
	static {
		Utils.clearDirectory(CustomLogger.getLoggingPath());
	}
	Logger logger = CustomLogger.getLogger(TimeSeriesTest.class);
	
	@Test
	public void doReprice(){
		TestUtils parserUtils = new TestUtils ();
		Set <Trade> trades = parserUtils.getRepriceableTrade();
		// Input the criteria here
		final String TAXONOMY = "InterestRate:IRSwap:FixedFloat";
		final String CCY = "EUR";
		final ZonedDateTime EFFECTIVEDATESTART = DateUtils.getUTCDate(2014, 8, 9);
		final ZonedDateTime EFFECTIVEDATEEND = DateUtils.getUTCDate(2014, 8, 15);
		final ZonedDateTime ENDDATESTART = DateUtils.getUTCDate(2019, 8, 7);
		final ZonedDateTime ENDDATEEND = DateUtils.getUTCDate(2019, 8, 20);

		int count = 0;
		for (Trade trade : trades) {
			String taxonomy = trade.getTaxonomy();
			String ccy = trade.getSettlement_currency();
			ZonedDateTime effectiveDate = epochToDateMidnight(trade.getEffective_date());
			ZonedDateTime endDate = epochToDateMidnight(trade.getEnd_date());
			
			if (taxonomy.equalsIgnoreCase(TAXONOMY) && ccy.equalsIgnoreCase(CCY) && effectiveDate.isAfter(EFFECTIVEDATESTART)
					&& effectiveDate.isBefore(EFFECTIVEDATEEND) && endDate.isAfter(ENDDATESTART) && endDate.isBefore(ENDDATEEND)){	
				reprice(trade);
				count++;
			}
		}
		logger.info(count);
	}
	
	private ZonedDateTime epochToDateMidnight (long epoch) {
		ZonedDateTime dateMidnight = ZonedDateTime.ofInstant(Instant.ofEpochSecond(epoch), ZoneId.of("UTC")).toLocalDate().atStartOfDay(ZoneId.of("UTC"));
		return dateMidnight;	
	}
	
	private int reprice(Trade trade){
		try{
			String taxonomy = trade.getTaxonomy();
			String settlementCurrency = trade.getSettlement_currency();
			
			// this should come from the instrument given timestamp is used in building the instrument 
			ZonedDateTime executionTimestamp = ZonedDateTime.ofInstant(Instant.ofEpochSecond(trade.getExecution_timestamp()), ZoneId.of("UTC"));
			
			Currency ccy = Currency.parse(settlementCurrency);
			List<Currency> ccyList = new ArrayList<>();
			ccyList.add(Currency.USD); 
			ccyList.add(Currency.EUR);
			ccyList.add(Currency.GBP);
			ccyList.add(Currency.JPY);
			ccyList.add(Currency.AUD);
			if (!ccyList.contains(ccy)){
				logger.info("Unsupported currency:" +ccy);
				return 0;
			}
			
			Swap<? ,?> instrument;
			
			// TODO: factor this out into InstrumentManager
/*			if (taxonomy.equalsIgnoreCase("InterestRate:IRSwap:FixedFloat")) {
				instrument = new SwapFixedIborBuilder(trade).getInstrument();
			} else if (taxonomy.equalsIgnoreCase("InterestRate:IRSwap:OIS")) {
				instrument = new SwapFixedONBuilder(trade).makeSwap();
			//} else if (taxonomy.equalsIgnoreCase("InterestRate:IRSwap:Basis")) {
				// TODO
			} else {
				logger.info("Unsupported trade type:" +taxonomy);
				return 0;
				//throw new IllegalArgumentException ("Unsupported trade type:" +ccy);
			}
			
			SingleCurrencyCurveMaker SCCM = new SingleCurrencyCurveMaker(ccy, executionTimestamp);
			Pair<MulticurveProviderDiscount, CurveBuildingBlockBundle> p = SCCM.makeCurve();
			MulticurveProviderDiscount curve = p.getFirst();
			
			PresentValueDiscountingCalculator PVC = PresentValueDiscountingCalculator.getInstance();
			ParSpreadMarketQuoteDiscountingCalculator PSMQC = ParSpreadMarketQuoteDiscountingCalculator.getInstance();
			
			MultipleCurrencyAmount newPv = instrument.accept(PVC, curve);
			long notional = trade.getRounded_notional_amount_1();
			double fixedRate = trade.getPrice_notation()/100;
			double percentageValue = newPv.getAmount(ccy) / notional;
			
			String additionalPriceNotationType = trade.getAdditional_price_notation_type();
			MultipleCurrencyAmount multiCcyUpfront;
			if (additionalPriceNotationType == null || additionalPriceNotationType.isEmpty()) {
				multiCcyUpfront = MultipleCurrencyAmount.of(ccy, 0.0);
			} else {
				Currency upfrontCcy = Currency.parse(trade.getAdditional_price_notation_type());
				double upfront = trade.getAdditional_price_notation();
				multiCcyUpfront = MultipleCurrencyAmount.of(upfrontCcy, upfront);
			}
			
			double FFRC = FairFixedRateCalculator.calculate(instrument, multiCcyUpfront, curve, executionTimestamp); 
			
			logger.info(executionTimestamp.getHour()+":"+executionTimestamp.getMinute()+":"+executionTimestamp.getSecond());*/
			//logger.info(fixedRate);
			//logger.info(FFRC+", "+trade.get("dissemination_id"));
			//logger.info(FFRC);
			
			return 1;
		}
		catch (Exception e){
			logger.info(trade.getSettlement_currency()+":  "+e.getMessage() + trade.getDissemination_id());
			e.printStackTrace();
			return -1;
		}
	}
}
