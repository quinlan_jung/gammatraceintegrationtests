package IdentificationTemp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Ignore;
import org.junit.Test;
import org.threeten.bp.Instant;
import org.threeten.bp.LocalDate;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZonedDateTime;

import com.gammatrace.datamodel.Trade;
import com.gammatrace.helpers.TestUtils;
import com.opengamma.analytics.financial.provider.calculator.discounting.PV01CurveParametersCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.ParRateDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueCurveSensitivityDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueParallelCurveSensitivityDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.generic.MarketQuoteSensitivityBlockCalculator;
import com.opengamma.analytics.financial.provider.calculator.sabrswaption.PresentValueCurveSensitivitySABRSwaptionCalculator;
import com.opengamma.analytics.financial.provider.curve.CurveBuildingBlockBundle;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderDiscount;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderInterface;
import com.opengamma.analytics.financial.provider.description.interestrate.SABRSwaptionProviderInterface;
import com.opengamma.analytics.financial.provider.sensitivity.parameter.ParameterSensitivityParameterCalculator;
import com.opengamma.analytics.util.amount.ReferenceAmount;
import com.opengamma.gammatrace.exceptions.UnsupportedOperation;
import com.opengamma.gammatrace.instrument.Instrument;
import com.opengamma.gammatrace.instrument.InstrumentBuildManager;
import com.opengamma.gammatrace.instrument.SwapInstrument;
import com.opengamma.gammatrace.marketconstruction.curve.CurveMaker;
import com.opengamma.gammatrace.marketconstruction.dictionary.DictionaryManager;
import com.opengamma.util.money.Currency;
import com.opengamma.util.time.DateUtils;
import com.opengamma.util.tuple.Pair;

@SuppressWarnings("rawtypes")
public class PackageIdentificationTest {
	private static final ParRateDiscountingCalculator PRDC = ParRateDiscountingCalculator.getInstance();
	private static final PV01CurveParametersCalculator<MulticurveProviderInterface> PV01CPC = new PV01CurveParametersCalculator<>(PresentValueCurveSensitivityDiscountingCalculator.getInstance());
	
	static MulticurveProviderDiscount usdCurve;
	static MulticurveProviderDiscount eurCurve;
	static MulticurveProviderDiscount gbpCurve;
	static MulticurveProviderDiscount jpyCurve;
	
	static {
		try {
			usdCurve = new CurveMaker(Currency.USD, DateUtils.getUTCDate(2014, 8, 8, 12, 0)).makeCurve().getFirst();
			eurCurve = new CurveMaker(Currency.EUR, DateUtils.getUTCDate(2014, 8, 8, 12, 0)).makeCurve().getFirst();
			gbpCurve = new CurveMaker(Currency.GBP, DateUtils.getUTCDate(2014, 8, 8, 12, 0)).makeCurve().getFirst();
			jpyCurve = new CurveMaker(Currency.JPY, DateUtils.getUTCDate(2014, 8, 8, 12, 0)).makeCurve().getFirst();
		} catch (Exception e) {
			// 
		}
	}
	
	private static HashMap<Currency, MulticurveProviderDiscount> curveMap = new HashMap<>();
	static {
		curveMap.put(Currency.USD, usdCurve);
		curveMap.put(Currency.EUR, eurCurve);
		curveMap.put(Currency.GBP, gbpCurve);
		curveMap.put(Currency.JPY, jpyCurve);
	}
	
	private static List<String> CURRENCIES = new ArrayList<>();
	static{
		CURRENCIES.add("USD");
		CURRENCIES.add("EUR");
		CURRENCIES.add("GBP");
		CURRENCIES.add("JPY");
	}
	
	@Test
	public void searchForCurveTrades(){
		TestUtils parserUtils = new TestUtils();
		Set<Trade> trades = parserUtils.getRepriceableTrade();
		Set<Pair<Trade, ReferenceAmount>> tradesWithDelta = calculateDelta(trades);
		HashMap<Long, List<Pair<Trade, ReferenceAmount>>> timestampToTrades = mapByTimestamp(tradesWithDelta);
		int counter = 0;
		for (Pair<Trade, ReferenceAmount> trade : tradesWithDelta) {						// this is a replacement for getting new trades real time
			//if (trade.getFirst().getDissemination_id() != 17103866L) continue; 
			List<Pair<Trade, ReferenceAmount>> timeCriteria = new ArrayList<>();
			List<Pair<Trade, ReferenceAmount>> exactCriteria = new ArrayList<>();
			List<Pair<Trade, ReferenceAmount>> finalList = new ArrayList<>();
			
			// apply timestamp interval filtering 
			int interval = 0;
			Long executionTimestamp = trade.getFirst().getExecution_timestamp();
			for (int i = 0; i < interval + 1; i++) {
				if (timestampToTrades.get(executionTimestamp - interval/2 + i) != null) {
					timeCriteria.addAll(timestampToTrades.get(executionTimestamp - interval/2 + i));			// add all trades with timestamp equal to time of the current iteration
				}
			}
			timeCriteria.remove(trade);
			if (timeCriteria.size() == 0) continue;												// if there are no candidate trades then skip this in the big loop and move to the next trade.
			// apply exact criteria
			for (Pair<Trade, ReferenceAmount> candidate : timeCriteria) {
				if (!candidate.getFirst().getEnd_date().equals(trade.getFirst().getEnd_date())) {} else {continue;}								// end dates must be different
				if (candidate.getFirst().getPrice_forming_continuation_data().equals(trade.getFirst().getPrice_forming_continuation_data())) {} else {continue;}			// must both be trades, or both terminations (doesn't seem to have much effect)
				if (candidate.getFirst().getEffective_date().equals(trade.getFirst().getEffective_date())) {} else {continue;}					// effective dates must be the same (???)
				if (candidate.getFirst().getNotional_currency_1().equals(trade.getFirst().getNotional_currency_1())) {} else {continue;}		// must be same currency
				if (sameExecutionDetails(candidate.getFirst(), trade.getFirst())) {} else {continue;}
				exactCriteria.add(candidate);
			}
			if (exactCriteria.size() == 0) continue;
			// approximate criteria			
			for (Pair<Trade, ReferenceAmount> candidate : exactCriteria) {
				if (similarDV01(candidate.getSecond(), trade.getSecond())) {} else {continue;}
				
				if (trade.getFirst().getPrice_forming_continuation_data().equalsIgnoreCase("Trade")) {			// if it's a new trade, would be unusual to have a fee paid
					if (!(candidate.getFirst().getAdditional_price_notation() == null)) {
						if (candidate.getFirst().getAdditional_price_notation() == 0) {} else {continue;}
					}
					if (!(trade.getFirst().getAdditional_price_notation() == null)) {
						if (trade.getFirst().getAdditional_price_notation() == 0) {} else {continue;}
					} 
				}
				
				finalList.add(candidate);
			}
			if (finalList.size() == 0) continue;
			Boolean sameTime = true;
			List<String> idList = new ArrayList<>();
			
			for (Pair<Trade, ReferenceAmount> matches : finalList) {
				LocalDate endDate = ZonedDateTime.ofInstant(Instant.ofEpochSecond(matches.getFirst().getEnd_date()), ZoneId.of("UTC")).toLocalDate();
				idList.add(matches.getFirst().getDissemination_id()+"  "+endDate+"  "+matches.getSecond().getMap().toString());
				
				sameTime = (matches.getFirst().getExecution_timestamp().equals(trade.getFirst().getExecution_timestamp()));
			}
			
			LocalDate endDate = ZonedDateTime.ofInstant(Instant.ofEpochSecond(trade.getFirst().getEnd_date()), ZoneId.of("UTC")).toLocalDate();
			System.out.println(sameTime+"  "+trade.getFirst().getDissemination_id()+"  "+endDate+"  "+trade.getSecond().getMap().toString()+idList);
			counter++;
		}
		System.out.println(counter);
	}
	
	
	private Set<Pair<Trade, ReferenceAmount>> calculateDelta(Set<Trade> tradeSet) {
		Set<Pair<Trade, ReferenceAmount>> result = new HashSet<>();
		for (Trade trade : tradeSet) {
			if (!CURRENCIES.contains(trade.getNotional_currency_1())) continue;
			if (!trade.getAction().equalsIgnoreCase("NEW")) continue;
			
			try {
				SwapInstrument instrument = (SwapInstrument) new InstrumentBuildManager(trade).makeInstrument();
				ReferenceAmount firstDelta = instrument.getDerivative().accept(PV01CPC, curveMap.get(instrument.getFirstNotional().getCurrency()));
				result.add(Pair.of(trade, firstDelta));
			} catch (Exception e) {
				//e.printStackTrace();
			}
		}
		return result;
	}
	
	private HashMap<Long, List<Pair<Trade, ReferenceAmount>>> mapByTimestamp(Set<Pair<Trade, ReferenceAmount>> tradeSet) {
		HashMap<Long, List<Pair<Trade, ReferenceAmount>>> timestampToTrades = new HashMap<>();
		for (Pair<Trade, ReferenceAmount> trade : tradeSet) {
			ArrayList<Pair<Trade, ReferenceAmount>> tradeList = new ArrayList<>();
			tradeList.add(trade);
			if (timestampToTrades.get(trade.getFirst().getExecution_timestamp()) == null) { // if it doesn't contain this key
				timestampToTrades.put(trade.getFirst().getExecution_timestamp(), tradeList);
			} else {
				List<Pair<Trade, ReferenceAmount>> tempList = timestampToTrades.get(trade.getFirst().getExecution_timestamp());
				tempList.add(trade);
				timestampToTrades.put(trade.getFirst().getExecution_timestamp(), tempList);
			}
		}
		return timestampToTrades;
	}
	
	private Boolean similarDV01(ReferenceAmount first, ReferenceAmount second) {
		Map.Entry<Pair<String, Currency>, Double> firstMaxDelta = null; 
		HashMap<Pair<String, Currency>, Double> firstDelta = first.getMap();
		for (Map.Entry<Pair<String, Currency>, Double> entry : firstDelta.entrySet()) {
			if (firstMaxDelta == null) {
				firstMaxDelta = entry;
			} else if (Math.abs(entry.getValue()) > firstMaxDelta.getValue()){
				firstMaxDelta = entry;
			}
		}
		
		Map.Entry<Pair<String, Currency>, Double> secondMaxDelta = null;
		HashMap<Pair<String, Currency>, Double> secondDelta = second.getMap();
		for (Map.Entry<Pair<String, Currency>, Double> entry : secondDelta.entrySet()) {
			if (secondMaxDelta == null) {
				secondMaxDelta = entry;
			} else if (Math.abs(entry.getValue()) > secondMaxDelta.getValue()){
				secondMaxDelta = entry;
			}
		}
		// if they have different max risk, then reject
		if (!firstMaxDelta.getKey().equals(secondMaxDelta.getKey())) {
			return false;
		}
		// if the magnitude of the risk isnt close, then reject
		if (Math.abs(firstMaxDelta.getValue()) > Math.abs(secondMaxDelta.getValue()) * 1.05 || Math.abs(firstMaxDelta.getValue()) < Math.abs(secondMaxDelta.getValue()) * 0.95) {
			return false;
		}
		return true;
	}
	
	private Boolean sameExecutionDetails(Trade firstTrade, Trade secondTrade) {
		if (!firstTrade.getCleared().equalsIgnoreCase(secondTrade.getCleared())) return false;
		if (!firstTrade.getIndication_of_collateralization().equalsIgnoreCase(secondTrade.getIndication_of_collateralization())) return false;
		if (!firstTrade.getIndication_of_end_user_exception().equalsIgnoreCase(secondTrade.getIndication_of_end_user_exception())) return false;
		if (!firstTrade.getExecution_venue().equalsIgnoreCase(secondTrade.getExecution_venue())) return false;
		return true;
	}
	
	/*
	@Test
	public void searchForPackages(){
		TestUtils parserUtils = new TestUtils();
		Set <Trade> trades = parserUtils.getRepriceableTrade();
		// calculate delta
	
		HashMap<Long, List<Trade>> timestampToTrades = mapByTimestamp(trades);
		
		List<Trade> timeCriteria = new ArrayList<Trade>();
		List<Trade> exactCriteria = new ArrayList<Trade>();
		List<Trade> finalList = new ArrayList<Trade>();
		for (Trade trade : trades) {						// this is a replacement for getting new trades real time
			if (!CURRENCIES.contains(trade.getNotional_currency_1())) continue;
			// apply timestamp interval filtering 
			int interval = 60;
			Long executionTimestamp = trade.getExecution_timestamp();
			for (int i = 0; i < interval + 1; i++) {
				timeCriteria.addAll(timestampToTrades.get(executionTimestamp - interval/2 + i));			// add all trades with timestamp equal to time of the current iteration
			}
			// apply exact criteria
			for (Trade candidate : timeCriteria) {
				if (candidate.getNotional_currency_1().equals(trade.getNotional_currency_1())) {} else {continue;}		// must be same currency
				if (sameExecutionDetails(candidate, trade)) {} else {continue;}
				exactCriteria.add(candidate);
			}
			// apply approximate criteria
			for (Trade candidate : exactCriteria) {
				
				
			}
		}
		
		
		List<Pair<Trade, Trade>> curveTrades = new ArrayList<>();
		for (Map.Entry<Long, List<Trade>> entry : timestampToTrades.entrySet()) {
			if (entry.getValue().size() == 2) {
				List<Trade> tradeIds = entry.getValue();
				Trade first = tradeIds.get(0);
				Trade second = tradeIds.get(1);
				
				// 
				if (first.getNotional_currency_1().equals(second.getNotional_currency_1()) && CURRENCIES.contains(first.getNotional_currency_1())) {} else {continue;}		// must be same currency (and we have this ccy)
				if (first.getAction().equalsIgnoreCase("NEW") && second.getAction().equalsIgnoreCase("NEW")) {} else {continue;}
				if (!first.getEnd_date().equals(second.getEnd_date())) {} else {continue;}
				if (first.getTaxonomy().equalsIgnoreCase("InterestRate:IRSwap:FixedFloat") && second.getTaxonomy().equalsIgnoreCase("InterestRate:IRSwap:FixedFloat")) {} else {continue;}
				
				curveTrades.add(Pair.of(first, second));
				System.out.println(first.getDissemination_id()+"  "+second.getDissemination_id());
				
				// same underlying libor
				// allow approximate equals of timestamp
				// price indicator of opposite directions
				// clearing, venue, collateralisation, end user,
				// certain packages are common e.g. 5y-10y, not e.g. 1m-3.5y
			}
		}
		System.out.println(curveTrades.size());
		
		ArrayList<String> results = new ArrayList<>();
		int counter = 0;
		for (Pair<Trade, Trade> eachPackage : curveTrades) {
			if (counter < 20) {
				try {
					InstrumentBuildManager firstBuild = new InstrumentBuildManager(eachPackage.getFirst());
					InstrumentBuildManager secondBuild = new InstrumentBuildManager(eachPackage.getSecond());
					SwapInstrument firstInstrument = (SwapInstrument) firstBuild.makeInstrument();
					SwapInstrument secondInstrument = (SwapInstrument) secondBuild.makeInstrument();

					CurveMaker SCCM = DictionaryManager.makeDictionary(firstInstrument);
					MulticurveProviderDiscount multicurves = SCCM.makeCurve().getFirst();						// should only have to do this once as execution timestamp is the same
					results.add(eachPackage.getFirst().getDissemination_id()+"  "+eachPackage.getSecond().getDissemination_id()+"  "+
							firstInstrument.getDerivative().accept(PV01CPC, multicurves).getMap().toString()+"   "+secondInstrument.getDerivative().accept(PV01CPC, multicurves).getMap().toString());
				} catch (Exception e) {
					System.out.println(eachPackage.getFirst().getDissemination_id() +":  "+e.getMessage());
					e.printStackTrace();
				}
				for (String s : results) {
					System.out.println(s);
				}
				
				
			}
			counter++;
		}
		
	}
	*/
	
}
