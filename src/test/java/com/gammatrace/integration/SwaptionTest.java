package com.gammatrace.integration;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.threeten.bp.Instant;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZonedDateTime;

import com.gammatrace.datamodel.Trade;
import com.gammatrace.helpers.TestUtils;
import com.opengamma.analytics.financial.interestrate.swaption.derivative.SwaptionCashFixedIbor;
import com.opengamma.analytics.financial.interestrate.swaption.derivative.SwaptionPhysicalFixedIbor;
import com.opengamma.analytics.financial.provider.calculator.discounting.ParRateDiscountingCalculator;
import com.opengamma.analytics.financial.provider.curve.CurveBuildingBlockBundle;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderDiscount;
import com.opengamma.analytics.financial.provider.description.interestrate.SABRSwaptionProviderDiscount;
import com.opengamma.gammatrace.instrument.Instrument;
import com.opengamma.gammatrace.instrument.InstrumentBuildManager;
import com.opengamma.gammatrace.instrument.InstrumentDescription;
import com.opengamma.gammatrace.instrument.SwapInstrument;
import com.opengamma.gammatrace.instrument.SwaptionInstrument;
import com.opengamma.gammatrace.marketconstruction.curve.CurveMaker;
import com.opengamma.gammatrace.marketconstruction.dictionary.DictionaryManager;
import com.opengamma.gammatrace.marketconstruction.sabr.SABRSwaptionCurveMaker;
import com.opengamma.gammatrace.rates.calculator.BlackImpliedVolatilityCalculator;
import com.opengamma.gammatrace.rates.calculator.MarketStandardFairFixedRateCalculator;
import com.opengamma.util.tuple.Pair;

public class SwaptionTest {
	private static final ParRateDiscountingCalculator PRDC = ParRateDiscountingCalculator.getInstance();
	private static List<String> CURRENCIES = new ArrayList<>();
	static{
		CURRENCIES.add("USD");
		CURRENCIES.add("EUR");
		CURRENCIES.add("GBP");
		//CURRENCIES.add("JPY");
	}
	
	@Test
	public void doReprice(){
		TestUtils parserUtils = new TestUtils();
		Set <Trade> trades = parserUtils.getRepriceableTrade();
		int counter = 0;
		
		ArrayList<String> results = new ArrayList<>();
		ArrayList<String> errors = new ArrayList<>();
		for (Trade trade : trades) {
			long id = trade.getDissemination_id();
			String taxonomy = trade.getTaxonomy();
			String currency = trade.getNotional_currency_1();
			if (taxonomy.equalsIgnoreCase("InterestRate:Option:Swaption") && CURRENCIES.contains(currency) && counter < 10) {	
				try{
					InstrumentBuildManager build = new InstrumentBuildManager(trade);
					SwaptionInstrument instrument = (SwaptionInstrument) build.makeInstrument();
					SABRSwaptionCurveMaker curveMaker = new SABRSwaptionCurveMaker(instrument.getSecondNotional().getCurrency(), instrument.getExecutionTimestamp());
					SABRSwaptionProviderDiscount curve = curveMaker.makeSABRSwaptionCurve();
					// price metrics 
					Double volatility;
					if (instrument.getInstrumentDescription().equals(InstrumentDescription.FIXED_IBOR_CASH_SWAPTION) || instrument.getInstrumentDescription().equals(InstrumentDescription.FIXED_IBOR_STRADDLE_CASH_SWAPTION)) {
						SwaptionCashFixedIbor[] derivative = (SwaptionCashFixedIbor[]) instrument.getDerivative();
						volatility = BlackImpliedVolatilityCalculator.getImpliedVolatility(derivative, instrument.getSpotOptionPremium(), curve.getMulticurveProvider());
					} else {
						SwaptionPhysicalFixedIbor[] derivative = (SwaptionPhysicalFixedIbor[]) instrument.getDerivative();
						volatility = BlackImpliedVolatilityCalculator.getImpliedVolatility(derivative, instrument.getSpotOptionPremium(), curve.getMulticurveProvider());
					}
					
					ZonedDateTime effectiveDate = ZonedDateTime.ofInstant(Instant.ofEpochSecond(trade.getEffective_date()), ZoneId.of("UTC")).toLocalDate().atStartOfDay(ZoneId.of("UTC"));
					ZonedDateTime endDate = ZonedDateTime.ofInstant(Instant.ofEpochSecond(trade.getEnd_date()), ZoneId.of("UTC")).toLocalDate().atStartOfDay(ZoneId.of("UTC"));
					results.add("ID: "+trade.getDissemination_id()+", CCY: "+currency+", effective: "+effectiveDate+", end: "+endDate+", premium: "+trade.getOption_premium()+
								" vol: "+volatility);						
				}
				catch (Exception e){
					System.out.println(trade.getDissemination_id() +":  "+e.getMessage());
					e.printStackTrace();
					errors.add(trade.getDissemination_id()+" "+e.toString());
				}
				counter++;
			}
		}
	
		System.out.println(counter);
		for (String s : results.toArray(new String[results.size()])) {
			System.out.println(s);
		}
		System.out.println(results.size());
		for (String s : errors.toArray(new String[errors.size()])) {
			System.out.println(s);
		}
	}
}
