package IdentificationTemp;

import gammatrace.criteria.DifferentEndDates;
import gammatrace.criteria.SameExecutionDetails;
import gammatrace.criteria.SameTimestamp;
import gammatrace.criteria.SimilarDV01;
import gammatrace.fixedfloatpositions.OutrightPosition;
import gammatrace.fixedfloatpositions.Position;
import gammatrace.repricedtrades.RepricedIRSwap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import com.gammatrace.datamodel.Trade;
import com.gammatrace.helpers.TestUtils;
import com.opengamma.gammatrace.prod.RepriceResult;
import com.opengamma.gammatrace.prod.Repricer;

public class SwaptionDeltaHedgeTest {
	private static List<String> CURRENCIES = new ArrayList<>();
	static{
		CURRENCIES.add("USD");
		CURRENCIES.add("EUR");
		CURRENCIES.add("GBP");
		CURRENCIES.add("JPY");
	}
	
	@Test
	public void searchForHedgedSwaptions() {
		SameTimestamp sameTimestamp = new SameTimestamp();
		DifferentEndDates differentEndDates = new DifferentEndDates();
		SameExecutionDetails sameExecutionDetails = new SameExecutionDetails();
		SimilarDV01 similarDV01 = new SimilarDV01();
		
		TestUtils parserUtils = new TestUtils();
		Set<Trade> trades = parserUtils.getRepriceableTrade();
		int counter = 0;
		
		// build the set of RepricedTrades (imagine this came from the database in reality)
		Set<Position> swaptionDatabase = new HashSet<>();
		HashMap<Position, Set<Position>> swapCandidates = new HashMap<>(); 
		for (Trade trade : trades) {
			if (trade.getTaxonomy().equalsIgnoreCase("InterestRate:Option:Swaption") && CURRENCIES.contains(trade.getNotional_currency_1()) ) {
				counter++;
				//RepriceResult repriceResult = reprice(trade);
				RepriceResult repriceResult = new RepriceResult();
				RepricedIRSwap repricedTrade = new RepricedIRSwap(trade, repriceResult);
				Position swaptionPosition = new OutrightPosition(repricedTrade);
				swaptionDatabase.add(swaptionPosition);
				
				Set<Position> swaps = new HashSet<>();
				for (Trade swap : trades) {
					if (trade.getTaxonomy().equalsIgnoreCase("InterestRate:IRSwap:FixedFloat")) {
						if (!trade.getExecution_timestamp().equals(swap.getExecution_timestamp())) continue;
						//if (!trade.getPrice_notation().equals(swap.getPrice_notation())) continue;
						if (!trade.getNotional_currency_1().equals(swap.getNotional_currency_1())) continue;
						//if (!trade.getEnd_date().equals(swap.getEnd_date())) continue;
						//if (!trade.getEffective_date().equals(swap.getEffective_date())) continue;
						swaps.add(new OutrightPosition(new RepricedIRSwap(swap, new RepriceResult())));		// empty results
					}
				}
				if (swaps.size() > 0) {
					swapCandidates.put(swaptionPosition, swaps);
				}
			}
			
		}
		
		System.out.println(swapCandidates.toString());
	}
	
	private RepriceResult reprice(Trade trade){
		try{
			return Repricer.reprice(trade);	
		}
		catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
}
