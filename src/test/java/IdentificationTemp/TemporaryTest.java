package IdentificationTemp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import gammatrace.criteria.ButterflyAnalyser;
import gammatrace.criteria.DifferentEndDates;
import gammatrace.criteria.SameExecutionDetails;
import gammatrace.criteria.SameTimestamp;
import gammatrace.criteria.SimilarDV01;
import gammatrace.criteria.SteepenerAnalyser;
import gammatrace.direction.DirectionAnalyser;
import gammatrace.fixedfloatpositions.ButterflyPosition;
import gammatrace.fixedfloatpositions.OutrightPosition;
import gammatrace.fixedfloatpositions.Position;
import gammatrace.fixedfloatpositions.PositionSetBuilder;
import gammatrace.fixedfloatpositions.SteepenerPosition;
import gammatrace.repricedtrades.RepricedIRSwap;
import gammatrace.repricedtrades.RepricedOption;
import gammatrace.repricedtrades.RepricedTrade;
import gammatrace.repricedtrades.RepricedXCcySwap;

import org.junit.Test;

import com.gammatrace.datamodel.Trade;
import com.gammatrace.helpers.TestUtils;
import com.opengamma.analytics.financial.provider.calculator.discounting.PV01CurveParametersCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.ParRateDiscountingCalculator;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueCurveSensitivityDiscountingCalculator;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderInterface;
import com.opengamma.gammatrace.prod.RepriceResult;
import com.opengamma.gammatrace.prod.Repricer;

public class TemporaryTest {
	private static final ParRateDiscountingCalculator PRDC = ParRateDiscountingCalculator.getInstance();
	private static final PV01CurveParametersCalculator<MulticurveProviderInterface> PV01CPC = new PV01CurveParametersCalculator<>(PresentValueCurveSensitivityDiscountingCalculator.getInstance());
	
	private static List<String> CURRENCIES = new ArrayList<>();
	static{
		CURRENCIES.add("USD");
		CURRENCIES.add("EUR");
		CURRENCIES.add("GBP");
		CURRENCIES.add("JPY");
	}
	
	@Test
	public void searchForCurveTrades(){
		
		/*
		 * This part just prices a few trades
		 */
		TestUtils parserUtils = new TestUtils();
		Set<Trade> trades = parserUtils.getRepriceableTrade();
		int counter = 0;
		
		// build the set of RepricedTrades (imagine this came from the database in reality)
		HashMap<Long, RepricedIRSwap> database = new HashMap<>();
		HashSet<RepricedIRSwap> newTradeSet = new HashSet<>();	// this is purely to simulate receiving new trades
		for (Trade trade : trades) {
			if (trade.getTaxonomy().equalsIgnoreCase("InterestRate:IRSwap:FixedFloat") && CURRENCIES.contains(trade.getNotional_currency_1()) && counter < 10) {
				RepriceResult repriceResult = reprice(trade);
				RepricedIRSwap repricedTrade = new RepricedIRSwap(trade, repriceResult);
				database.put(trade.getDissemination_id(), repricedTrade);
				newTradeSet.add(repricedTrade);
				counter++;
			}
		}
		
		// build positions from the database of RepricedTrade's  (this should actually go after the SQL query, but have to do it here bc the criteria work on Set<Position> not Set<RepricedTrade>
		Set<Position<RepricedIRSwap>> positionDatabase = PositionSetBuilder.buildPositionSet(database);

		for (Position<RepricedIRSwap> p : positionDatabase) {
			System.out.println(p.getClass());
		}
		
		// check for SteepenerPosition.. 
		//imagine we're getting new trades and repricing them ... simulate this by just iterating over the database
		for (RepricedTrade repricedTrade : newTradeSet) {
			if (repricedTrade instanceof RepricedIRSwap) {			// actually only if it's a fixed/float we do the package analysis 
				Position<RepricedIRSwap> currentPosition = new OutrightPosition((RepricedIRSwap) repricedTrade);
				Set<Position<RepricedIRSwap>> simulatedDatabase = new HashSet<>(); 		
				simulatedDatabase.addAll(positionDatabase);
				simulatedDatabase.remove(currentPosition);
				
				Set<? extends Position<RepricedIRSwap>> querySet;
				// make SQL query here with these criteria
				SameTimestamp<RepricedIRSwap> sameTimestamp = new SameTimestamp<>();				
				DifferentEndDates<RepricedIRSwap> differentEndDates = new DifferentEndDates<>();
				SameExecutionDetails<RepricedIRSwap> sameExecutionDetails = new SameExecutionDetails<>();
				querySet = sameTimestamp.meetsCriteria(repricedTrade, simulatedDatabase);
				querySet = differentEndDates.meetsCriteria(repricedTrade, querySet);
				querySet = sameExecutionDetails.meetsCriteria(repricedTrade, querySet);
				// THIS IS HOW THINGS SHOULD LOOK AT THE BEGINNING 
				System.out.println(repricedTrade.getDisseminationId());
				
				for (Position<RepricedIRSwap> p : querySet) {
					System.out.print(p.getClass()+"   ");
					for (RepricedIRSwap s : p.getRepricedTrades()) {
						System.out.print(s.getDisseminationId()+"  ");
					}
					System.out.println("");
				}
				System.out.println("----------------");
				SteepenerAnalyser steepenerAnalyser = new SteepenerAnalyser(currentPosition, querySet);
				Set<? extends Position<RepricedIRSwap>> updatedSet = steepenerAnalyser.getUpdatedSet();
				currentPosition = steepenerAnalyser.getUpdatedPosition();
				
				for (Position<RepricedIRSwap> p : updatedSet) {
					System.out.print(p.getClass()+"   ");
					for (RepricedIRSwap s : p.getRepricedTrades()) {
						System.out.print(s.getDisseminationId()+"  ");
					}	
					System.out.println("");
				}
				System.out.print(currentPosition.getClass()+"   ");
				for (RepricedIRSwap s : currentPosition.getRepricedTrades()) {
					System.out.print(s.getDisseminationId()+"  ");
				}
				System.out.println("");
				
				System.out.println("----------------");
				ButterflyAnalyser butterflyAnalyser = new ButterflyAnalyser(currentPosition, updatedSet);
				updatedSet = butterflyAnalyser.getUpdatedSet();
				currentPosition = butterflyAnalyser.getUpdatedPosition();
				
				for (Position<RepricedIRSwap> p : updatedSet) {
					System.out.print(p.getClass()+"   ");
					for (RepricedIRSwap s : p.getRepricedTrades()) {
						System.out.print(s.getDisseminationId()+"  ");
					}	
					System.out.println("");
				}
				System.out.print(currentPosition.getClass()+"   ");
				for (RepricedIRSwap s : currentPosition.getRepricedTrades()) {
					System.out.print(s.getDisseminationId()+"  ");
				}
				System.out.println("");
				System.out.println("--------------------");
			
			
				// finally, fix the direction and add to the set of Position's to be written back into the database
				//System.out.println(currentPosition.getIsQuotePayer());
				System.out.println("Before "+currentPosition.getIsQuotePayer());
				currentPosition = (Position<RepricedIRSwap>) DirectionAnalyser.fixDirection(currentPosition);
				System.out.println("After "+currentPosition.getIsQuotePayer());
				System.out.println("=====================");
		
			} else if (repricedTrade instanceof RepricedOption) {
				// TODO: just do the direction analysis for now
			}
			
		}
		
		
		
	}
	
	private RepriceResult reprice(Trade trade){
		try{
			return Repricer.reprice(trade);	
		}
		catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
}
