package com.gammatrace.integration;

import org.junit.Ignore;
import org.junit.Test;
import org.threeten.bp.Period;
import org.threeten.bp.ZonedDateTime;

import com.opengamma.analytics.financial.forex.method.FXMatrix;
import com.opengamma.analytics.financial.instrument.InstrumentDefinition;
import com.opengamma.analytics.financial.instrument.index.GeneratorAttributeFX;
import com.opengamma.analytics.financial.instrument.index.GeneratorAttributeIR;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapFixedON;
import com.opengamma.analytics.financial.instrument.index.GeneratorSwapXCcyIborIbor;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedIborDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapFixedONDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapIborIborDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapIborONDefinition;
import com.opengamma.analytics.financial.instrument.swap.SwapXCcyIborIborDefinition;
import com.opengamma.analytics.financial.interestrate.InstrumentDerivative;
import com.opengamma.analytics.financial.provider.calculator.discounting.ParSpreadMarketQuoteDiscountingCalculator;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderDiscount;
import com.opengamma.analytics.financial.schedule.ScheduleCalculator;
import com.opengamma.analytics.util.time.TimeCalculator;
import com.opengamma.financial.convention.businessday.BusinessDayConventionFactory;
import com.opengamma.financial.convention.calendar.Calendar;
import com.opengamma.financial.convention.calendar.MondayToFridayCalendar;
import com.opengamma.gammatrace.marketconstruction.curve.CurveMaker;
import com.opengamma.gammatrace.marketconstruction.curve.MultiCurrencyCurveMarketDataProvider;
import com.opengamma.gammatrace.ogextension.PresentValueDiscountingCalculator;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapFixedONTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.GeneratorSwapXCcyIborIborTemplate;
import com.opengamma.gammatrace.rates.generatortemplate.Generators;
import com.opengamma.gammatrace.rates.instrument.FixingBuilder;
import com.opengamma.util.money.Currency;
import com.opengamma.util.time.DateUtils;

public class CurveBuildingInterpolationTest {
	private static final Calendar NYC = new MondayToFridayCalendar("NYC");
	
	private static final ZonedDateTime CURVE_TIME = DateUtils.getUTCDate(2014, 8, 8, 0, 0);
	private static final Currency FIRST_CCY = Currency.USD;
	private static final Currency SECOND_CCY = Currency.EUR;
	
	private static final CurveMaker curveMaker = new CurveMaker(FIRST_CCY, SECOND_CCY, CURVE_TIME); 
	private static final MulticurveProviderDiscount multicurves = curveMaker.makeCurve().getFirst();
	private static final FXMatrix FX = multicurves.getFxRates();
	
	private static final PresentValueDiscountingCalculator PVDC = PresentValueDiscountingCalculator.getInstance();
	private static final ParSpreadMarketQuoteDiscountingCalculator PSMQDC = ParSpreadMarketQuoteDiscountingCalculator.getInstance();
	
	private static final GeneratorSwapFixedON USD1YFEDFUND = GeneratorSwapFixedONTemplate.getInstance().getGenerator(Generators.USD1YFEDFUND, NYC);
	private static final GeneratorSwapXCcyIborIbor EUR3MUSD3M = GeneratorSwapXCcyIborIborTemplate.getInstance().getGenerator(Generators.EUR3MUSD3M, NYC, NYC);
	
	@Test 
	@Ignore 
	public void shouldRepricePoints() {
		MultiCurrencyCurveMarketDataProvider CMDP = new MultiCurrencyCurveMarketDataProvider(FIRST_CCY, SECOND_CCY, CURVE_TIME);
		InstrumentDefinition<?>[][][] definitions = CMDP.getDefinitions();
		for (InstrumentDefinition<?>[][] eachUnit : definitions) {
			for (InstrumentDefinition<?>[] eachCurve : eachUnit) {
				for (InstrumentDefinition<?> eachInstrument : eachCurve) {
					System.out.println(convert(eachInstrument, CURVE_TIME).accept(PVDC, multicurves));		// at start of day etc. missing
				}
			}
		}
	}
	
	@Test
	@Ignore
	public void shouldInterpolateUSDOISSwaps() {
		int maxMonthlyYear = 3;
		int maxYearlyYear = 30;
		Period endPeriod;
		for (int i = 1; i <= maxYearlyYear; i++) {
			if (i <= maxMonthlyYear) {
				for (int j = 1; j <= maxMonthlyYear * 12; j++) {
					endPeriod = Period.ofMonths(j);
					GeneratorAttributeIR tenorAttribute = new GeneratorAttributeIR(endPeriod);
					InstrumentDefinition<?> definition = USD1YFEDFUND.generateInstrument(CURVE_TIME, 0.0, 1.0, tenorAttribute);
					double time = TimeCalculator.getTimeBetween(CURVE_TIME, ScheduleCalculator.getAdjustedDate(CURVE_TIME, endPeriod, BusinessDayConventionFactory.INSTANCE.getBusinessDayConvention("Modified Following"), NYC));
					System.out.println(convert(definition, CURVE_TIME).accept(PSMQDC, multicurves));
					//System.out.println(time);
					//System.out.println(time+","+convert(definition, CURVE_TIME).accept(PSMQDC, multicurves));
				}
				i = maxMonthlyYear;
			}
			endPeriod = Period.ofYears(i);
			GeneratorAttributeIR tenorAttribute = new GeneratorAttributeIR(endPeriod);
			InstrumentDefinition<?> definition = USD1YFEDFUND.generateInstrument(CURVE_TIME, 0.0, 1.0, tenorAttribute);
			double time = TimeCalculator.getTimeBetween(CURVE_TIME, ScheduleCalculator.getAdjustedDate(CURVE_TIME, endPeriod, BusinessDayConventionFactory.INSTANCE.getBusinessDayConvention("Modified Following"), NYC));
			System.out.println(convert(definition, CURVE_TIME).accept(PSMQDC, multicurves));
			//System.out.println(time);
			//System.out.println(time+","+convert(definition, CURVE_TIME).accept(PSMQDC, multicurves));
		}
	}
	
	@Test
	public void shouldInterpolateBasis() {
		int maxMonthlyYear = 3;
		int maxYearlyYear = 30;
		Period endPeriod;
		for (int i = 1; i <= maxYearlyYear; i++) {
			if (i <= maxMonthlyYear) {
				for (int j = 1; j <= maxMonthlyYear * 12; j++) {
					endPeriod = Period.ofMonths(j);
					GeneratorAttributeFX tenorAttribute = new GeneratorAttributeFX(endPeriod, FX);
					InstrumentDefinition<?> definition = EUR3MUSD3M.generateInstrument(CURVE_TIME, 0.0, 1.0, tenorAttribute);
					double time = TimeCalculator.getTimeBetween(CURVE_TIME, ScheduleCalculator.getAdjustedDate(CURVE_TIME, endPeriod, BusinessDayConventionFactory.INSTANCE.getBusinessDayConvention("Modified Following"), NYC));
					System.out.println(convert(definition, CURVE_TIME).accept(PSMQDC, multicurves));
					//System.out.println(time);
					//System.out.println(time+","+convert(definition, CURVE_TIME).accept(PSMQDC, multicurves));
				}
				i = maxMonthlyYear;
			}
			endPeriod = Period.ofYears(i);
			GeneratorAttributeFX tenorAttribute = new GeneratorAttributeFX(endPeriod, FX);
			InstrumentDefinition<?> definition = EUR3MUSD3M.generateInstrument(CURVE_TIME, 0.0, 1.0, tenorAttribute);
			double time = TimeCalculator.getTimeBetween(CURVE_TIME, ScheduleCalculator.getAdjustedDate(CURVE_TIME, endPeriod, BusinessDayConventionFactory.INSTANCE.getBusinessDayConvention("Modified Following"), NYC));
			System.out.println(convert(definition, CURVE_TIME).accept(PSMQDC, multicurves));
			//System.out.println(time);
			//System.out.println(time+","+convert(definition, CURVE_TIME).accept(PSMQDC, multicurves));
		}
	}
	
	private static InstrumentDerivative convert(final InstrumentDefinition<?> instrument, final ZonedDateTime curveTime) {
		InstrumentDerivative ird;
	    if (instrument instanceof SwapFixedONDefinition) {
	    	ird = ((SwapFixedONDefinition) instrument).toDerivative(curveTime, FixingBuilder.makeFixingTimeSeries((SwapFixedONDefinition) instrument, curveTime));
	    } else if (instrument instanceof SwapFixedIborDefinition) {
	    	ird = ((SwapFixedIborDefinition) instrument).toDerivative(curveTime, FixingBuilder.makeFixingTimeSeries((SwapFixedIborDefinition) instrument, curveTime));
	    } else if (instrument instanceof SwapIborIborDefinition) {
	    	ird = ((SwapIborIborDefinition) instrument).toDerivative(curveTime, FixingBuilder.makeFixingTimeSeries((SwapIborIborDefinition) instrument, curveTime));
	    } else if (instrument instanceof SwapIborONDefinition) {
	    	ird = ((SwapIborONDefinition) instrument).toDerivative(curveTime, FixingBuilder.makeFixingTimeSeries((SwapIborONDefinition) instrument, curveTime));
	    } else if (instrument instanceof SwapXCcyIborIborDefinition ) {
	    	ird = ((SwapXCcyIborIborDefinition) instrument).toDerivative(curveTime, FixingBuilder.makeFixingTimeSeries((SwapXCcyIborIborDefinition) instrument, curveTime));
	    } else {
	    	ird = instrument.toDerivative(curveTime);
	    }
	    return ird;
	}
	
}
