package com.gammatrace.integration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import com.gammatrace.commonutils.Utils;
import com.gammatrace.datamodel.Trade;
import com.gammatrace.helpers.TestUtils;
import com.gammatrace.logging.CustomLogger;
import com.opengamma.util.tuple.Pair;

public class IborMappingTest {
	static{
		Utils.clearDirectory(CustomLogger.getLoggingPath());
	}
	Logger logger = CustomLogger.getLogger(IborMappingTest.class);
	
	@Test
	public void mapPairToAssetName(){
		try {
			TestUtils parserUtils = new TestUtils ();
			Set <Trade> trades = parserUtils.getRepriceableTrade();
			
			LinkedHashMap<Pair<String, String>, List<String>> pairToAssetName = new LinkedHashMap<>();
			
			for (Trade trade : trades) {
				String taxonomy = trade.getTaxonomy();
				if (taxonomy.equalsIgnoreCase("InterestRate:IRSwap:FixedFloat") || taxonomy.equalsIgnoreCase("InterestRate:IRSwap:OIS")){
					String assetName1 = trade.getUnderlying_asset_1();
					String assetName2 = trade.getUnderlying_asset_2();
					String settlementCurrency = trade.getSettlement_currency();
					String freq1 = trade.getReset_frequency_1_asPeriod() == null ? "" : trade.getReset_frequency_1_asPeriod().toString();
					String freq2 = trade.getReset_frequency_2_asPeriod() == null ? "" : trade.getReset_frequency_2_asPeriod().toString();
					if (!assetName1.equalsIgnoreCase("FIXED")){
						List<String> loopList = new ArrayList<>();
						if (pairToAssetName.containsKey(Pair.of(settlementCurrency, freq1))){
							loopList = pairToAssetName.get(Pair.of(settlementCurrency, freq1));
							if (!loopList.contains(assetName1)) {
								loopList.add(assetName1);
							}							
						} else {
							loopList.add(assetName1);
						}
						pairToAssetName.put(Pair.of(settlementCurrency, freq1), loopList);
					} else {
						List<String> loopList = new ArrayList<>();
						if (pairToAssetName.containsKey(Pair.of(settlementCurrency, freq2))){
							loopList = pairToAssetName.get(Pair.of(settlementCurrency, freq2));
							if (!loopList.contains(assetName2)) {
								loopList.add(assetName2);
							}
						} else {
							loopList.add(assetName2);
						}
						pairToAssetName.put(Pair.of(settlementCurrency, freq2), loopList);
					}
				}
			}
			
			
			List<Map.Entry<Pair<String, String>, List<String>>> sortList = new ArrayList<>(pairToAssetName.entrySet());
			Collections.sort(sortList, new Comparator<Map.Entry<Pair<String, String>, List<String>>>() {
				@Override
				public int compare(Map.Entry<Pair<String, String>, List<String>> o1, Map.Entry<Pair<String, String>, List<String>> o2) {
					return (o1.getKey().getFirst().compareTo(o2.getKey().getFirst()));
				}
			} );
			
			Map<Pair<String, String>, List<String>> sortedMap = new LinkedHashMap<>();
			for (Map.Entry<Pair<String, String>, List<String>> entry : sortList) {
				sortedMap.put(entry.getKey(), entry.getValue());
			}
			
			for (Map.Entry<Pair<String, String>, List<String>> e : sortedMap.entrySet()) {
				logger.info(e.toString());
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
