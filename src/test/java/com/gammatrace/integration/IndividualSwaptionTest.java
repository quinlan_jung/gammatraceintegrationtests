package com.gammatrace.integration;

import java.util.Set;

import org.junit.Test;

import com.gammatrace.datamodel.Trade;
import com.gammatrace.helpers.TestUtils;
import com.opengamma.analytics.financial.interestrate.InstrumentDerivative;
import com.opengamma.analytics.financial.interestrate.swaption.derivative.SwaptionPhysicalFixedIbor;
import com.opengamma.analytics.financial.provider.calculator.discounting.ParRateDiscountingCalculator;
import com.opengamma.analytics.financial.provider.description.interestrate.SABRSwaptionProviderDiscount;
import com.opengamma.gammatrace.instrument.Instrument;
import com.opengamma.gammatrace.instrument.InstrumentBuildManager;
import com.opengamma.gammatrace.instrument.SwaptionInstrument;
import com.opengamma.gammatrace.marketconstruction.sabr.SABRSwaptionCurveMaker;
import com.opengamma.gammatrace.prod.RepriceResult;
import com.opengamma.gammatrace.prod.Repricer;
import com.opengamma.gammatrace.rates.calculator.SABRSwaptionVegaCalculator;

public class IndividualSwaptionTest {
	private static final ParRateDiscountingCalculator PRDC = ParRateDiscountingCalculator.getInstance();
	@Test
	public void doReprice(){
		TestUtils parserUtils = new TestUtils();
		Set <Trade> trades = parserUtils.getRepriceableTrade();
		long swaption = 17105021;
		
		for (Trade trade : trades) {
			long id = trade.getDissemination_id();
			if (id == swaption) {					// <<<<---------------- INPUT ID HERE
				System.out.println("Found search id");
				try{
					InstrumentBuildManager build = new InstrumentBuildManager(trade);
					Instrument instrument = build.makeInstrument();
					SwaptionInstrument swaptionInstrument = (SwaptionInstrument) instrument;
					SABRSwaptionCurveMaker curveMaker = new SABRSwaptionCurveMaker(instrument.getSecondNotional().getCurrency(), instrument.getExecutionTimestamp());
					SABRSwaptionProviderDiscount curve = curveMaker.makeSABRSwaptionCurve();
					InstrumentDerivative[] straddle = swaptionInstrument.getDerivative();
					System.out.println(straddle[0].toString());
					
					SABRSwaptionVegaCalculator vegaCalculator = new SABRSwaptionVegaCalculator(swaptionInstrument, curve);
					System.out.println(vegaCalculator.getVegaMatrix().getAlpha().getVegaPoints().toString());
				}
				catch (Exception e){
					System.out.println(trade.getDissemination_id() +":  "+e.getMessage());
					e.printStackTrace();
				}
				
			}
		}
	
	
	
	}
}
