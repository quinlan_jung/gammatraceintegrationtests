package IdentificationTemp;

public class PseudoCode {
	// TODO: caching of trades with execution timestamp in the last X minutes? (low priority.. only if performance is bad?)  
	
	// get new data from DTCC, Bloomberg etc.
	
	// for each new trade
		// if the trade meets repricing criteria
			// reprice and create RepriceResult
			// use RepriceResult and Trade to create RepricedTrade
		// else 
			// TODO: how are we going to handle trades that can't be repriced?
	
		// if taxonomy is IRSwap:FixedFloat
			// use the RepricedIRSwap to build an OutrightPosition
			// Query database for all trades with same execution timestamp, execution details and different end dates, and use the data to build a set of RepricedIRSwap 
			// Use PositionSetBuilder to convert to set of Position
			
			// use SteepenerAnalyser to update the set of Position's and update the Position object of the current trade
			// use ButterflyAnalyser to update the set of Position's and update the Position object of the current trade

			// if the updated set of Position's is different to the original queried set (will only ever remove 1 Position from this set)
				// if removed Position is an OutrightPosition
					// remove from the repriced Fixed/Float table the RepricedIRSwap corresponding to the removed Position  
				// else 
					// remove from the repriced Fixed/Float table the group of RepricedIRSwaps corresponding to the removed Position, and remove from the Steepener table the associated entry
	
	
	
		// use SwapDirection to fix the direction of the current Position
	
		// if current Position is an OutrightPosition
			// insert to the repriced Fixed/Float table the RepricedIRSwap corresponding to the current Position
		// else 
			// insert to the repriced Fixed/Float table the group of RepricedIRSwaps corresponding to the removed Position, and insert to the Steepener/ Butterfly table the new entry
	
	
}
