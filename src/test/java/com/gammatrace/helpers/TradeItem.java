package com.gammatrace.helpers;

import org.apache.log4j.Logger;
import org.joda.time.DateTimeConstants;

import com.gammatrace.logging.CustomLogger;

public class TradeItem {
	Logger logger = CustomLogger.getLogger(TradeItem.class);
	long disseminationId;
	String description;
	long startMillis = -1;
	long endMillis = -1;

	
	public TradeItem(long disseminationId, String description) {
		this.disseminationId = disseminationId;
		this.description = description;
	}
	
	public TradeItem(long disseminationId) {
		this.disseminationId = disseminationId;
	}
	
	public long getDisseminationId() {
		return disseminationId;
	}
	public void setDisseminationId(long disseminationId) {
		this.disseminationId = disseminationId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public void startTimer(){
		startMillis = System.currentTimeMillis();
	}
	
	public void endTimer(){
		endMillis = System.currentTimeMillis();
	}
	
	public double getTotalSecondsElapsed(){
		return (endMillis - startMillis) / (double) DateTimeConstants.MILLIS_PER_SECOND;
	}
	
	public void printResults(){
		logger.info(String.format("DissemId: %d Description: %s TotalRepriceTime: %f sec", disseminationId, description, getTotalSecondsElapsed()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (disseminationId ^ (disseminationId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TradeItem other = (TradeItem) obj;
		if (disseminationId != other.disseminationId)
			return false;
		return true;
	}
}
