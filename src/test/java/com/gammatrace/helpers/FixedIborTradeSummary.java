package com.gammatrace.helpers;

import java.text.DecimalFormat;

import org.threeten.bp.Instant;
import org.threeten.bp.LocalDate;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZoneOffset;
import org.threeten.bp.ZonedDateTime;

import com.gammatrace.datamodel.Trade;

public class FixedIborTradeSummary extends TradeItem {
	
	private DecimalFormat decimal = new DecimalFormat("0.0000");
	
	private LocalDate effectiveDate;
	private LocalDate endDate;
	private Double priceNotation;
	private Double fairFixedRate;
	private Double marketImpliedFixedRate;
	
	public FixedIborTradeSummary (Trade trade) {
		super(trade.getDissemination_id());
		effectiveDate = ZonedDateTime.ofInstant(Instant.ofEpochSecond(trade.getEffective_date()), ZoneId.of("UTC")).toLocalDate();
		endDate = ZonedDateTime.ofInstant(Instant.ofEpochSecond(trade.getEnd_date()), ZoneId.of("UTC")).toLocalDate();
		priceNotation = trade.getPrice_notation();
	}
	
	public void setFairFixedRate(Double d) {
		fairFixedRate = d;
	}
	public void setMarketImpliedFixedRate(Double d) {
		marketImpliedFixedRate = d;
	}
	
	public LocalDate getEffectiveDate() {
		return effectiveDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public Double getPriceNotation() {
		return priceNotation;
	}

	public Double getFairFixedRate() {
		return fairFixedRate;
	}
	
	@Override
	public void printResults() {
		System.out.println(getDisseminationId()+", "+effectiveDate+", "+decimal.format(priceNotation*100)+", "+decimal.format(fairFixedRate*100)+", "+decimal.format(marketImpliedFixedRate*100));
	}
	
}
