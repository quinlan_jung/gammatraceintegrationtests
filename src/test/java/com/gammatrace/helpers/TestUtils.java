package com.gammatrace.helpers;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import com.gammatrace.datamodel.Trade;
import com.gammatrace.filtermanager.DebugFilterManager;
import com.gammatrace.logging.CustomLogger;
import com.gammatrace.metricmanager.AbstractMetricManager;
import com.gammatrace.metricmanager.DummyMetricManager;
import com.gammatrace.parser.Sorter;

public class TestUtils {
	Logger logger = CustomLogger.getLogger(TestUtils.class);

	/**
	 * Takes an entire day's worth of trades and returns it as a list of Maps
	 */
	public Set <Trade> getRepriceableTrade() {
		try{
			// Create a new database client and a DTCC repricer
			DebugFilterManager filterManager = new DebugFilterManager();
			Sorter sorter = new Sorter();
			AbstractMetricManager metricManager = new DummyMetricManager(filterManager);
			
			//BufferedReader br = new BufferedReader(new FileReader("/Users/teamawesome/Documents/InterestSwap/BarebonesParser/resources/parserTestFiles/test_data.csv"));
			BufferedReader br = new BufferedReader(new FileReader("./resources/parserTestFiles/test_data.csv"));
			String line;
			int lineNumber = 1;
			logger.info("Starting the import");
			while ((line = br.readLine()) != null) {
			   if (lineNumber == 1){
				   lineNumber++;
				   continue;
			   }
			   sorter.processRecord(line);
			   lineNumber++;
			}
			logger.info("Finished the import");
			br.close();
			
			List<Map<String, Object>> insertRecords = sorter.getInsertRows();
			metricManager.filterAndCalculate(insertRecords);
			
			ObjectMapper mapper = new ObjectMapper();
			Set<Trade> trades = new HashSet<Trade> ();
			for (Map<String, Object> tradeMap : filterManager.getAcceptedtrades()){
				trades.add(mapper.convertValue(tradeMap, Trade.class));
			}
			return trades;
		}
		catch(Exception e){
			logger.error(e.getMessage());
		}
		return null;

	}
}
