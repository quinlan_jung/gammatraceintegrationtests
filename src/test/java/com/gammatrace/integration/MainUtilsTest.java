package com.gammatrace.integration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;
import org.threeten.bp.Period;
import org.threeten.bp.ZonedDateTime;

import com.gammatrace.commonutils.Utils;
import com.gammatrace.datamodel.Trade;
import com.gammatrace.helpers.TestUtils;
import com.gammatrace.logging.CustomLogger;
import com.opengamma.analytics.financial.interestrate.swap.derivative.Swap;
import com.opengamma.analytics.financial.interestrate.swaption.derivative.SwaptionPhysicalFixedIbor;
import com.opengamma.analytics.financial.provider.calculator.discounting.PresentValueDiscountingCalculator;
import com.opengamma.analytics.financial.provider.curve.CurveBuildingBlockBundle;
import com.opengamma.analytics.financial.provider.description.interestrate.MulticurveProviderDiscount;
import com.opengamma.gammatrace.instrument.Instrument;
import com.opengamma.gammatrace.instrument.InstrumentBuildManager;
import com.opengamma.gammatrace.instrument.SwapInstrument;
import com.opengamma.gammatrace.instrument.SwaptionInstrument;
import com.opengamma.gammatrace.marketconstruction.curve.CurveMaker;
import com.opengamma.gammatrace.marketconstruction.quote.QuoteInstrumentType;
import com.opengamma.gammatrace.prod.DeltaResults;
import com.opengamma.gammatrace.rates.calculator.DeltaRiskCalculatorOld;
import com.opengamma.gammatrace.rates.calculator.MarketStandardEquivalentSwapBuilder;
import com.opengamma.gammatrace.rates.calculator.MarketStandardFairFixedRateCalculator;
import com.opengamma.util.money.Currency;
import com.opengamma.util.money.MultipleCurrencyAmount;
import com.opengamma.util.tuple.Pair;

/**
 * This Test class reprices a days worth of trades
 */
public class MainUtilsTest {
	static {
		Utils.clearDirectory(CustomLogger.getLoggingPath());
	}
	Logger logger = CustomLogger.getLogger(MainUtilsTest.class);
	
	@Test
	public void foo(){
		TestUtils parserUtils = new TestUtils ();
		Set <Trade> trades = parserUtils.getRepriceableTrade();
		
		int i = 0;
		for (Trade trade : trades){
			if (i== 200) {
				break;
			}
			i++;
		}
	}

	@Test
	public void doReprice(){
		TestUtils parserUtils = new TestUtils ();
		Set <Trade> trades = parserUtils.getRepriceableTrade();
		
		int skipped = 0;
		int passed = 0;
		int failed = 0;
		int total = 0;
		
		for (Trade trade : trades) {
			// hack to get it to run quicker with just a few trades... obviously remove this
			if (total < 100) {
				
				int returnValue = reprice(trade);
				if (returnValue > 0){
					passed++;
				} else if (returnValue == 0) {
					skipped++;
				} else {
					failed++;
				}
				
				total++;
			} 	
		}
			
		logger.info("Passed: "+passed);
		logger.info("Failed: "+failed);
		logger.info("Skipped: "+skipped);
	}
	
	private int reprice(Trade trade){
		try{
			String taxonomy = trade.getTaxonomy();
			String settlementCurrency = trade.getSettlement_currency();
			
			// this should come from the instrument given timestamp is used in building the instrument 
			Currency ccy = Currency.parse(settlementCurrency);
			
			List<Currency> ccyList = new ArrayList<>();
			ccyList.add(Currency.USD); 
			ccyList.add(Currency.EUR);
			ccyList.add(Currency.GBP);
			ccyList.add(Currency.JPY);
			ccyList.add(Currency.AUD);
			if (!ccyList.contains(ccy)){
				logger.info("Unsupported currency:" +ccy);
				return 0;
			}
			
			//Build the Instrument
			InstrumentBuildManager build = new InstrumentBuildManager(trade);
			Instrument instrument = build.makeInstrument();
			if (instrument == null) {
				logger.info("Unsupported trade type: "+taxonomy);
				return 0;
			}

			ZonedDateTime executionTimestamp =instrument.getExecutionTimestamp();
			
			//Build the curve
			//TODO: from instrument work out what type of curve / mktdata needs to be built... market data manager? 
			
			CurveMaker SCCM;
			if (instrument.getSecondNotional().getCurrency().equals(instrument.getFirstNotional().getCurrency())) {
				SCCM = new CurveMaker(instrument.getSecondNotional().getCurrency(), executionTimestamp);
			} else {
				SCCM = new CurveMaker(instrument.getSecondNotional().getCurrency(), instrument.getFirstNotional().getCurrency(), executionTimestamp);
			}
			Pair<MulticurveProviderDiscount, CurveBuildingBlockBundle> pair = SCCM.makeCurve();
			MulticurveProviderDiscount curve = pair.getFirst();
			
			// calculate the price metrics
			
			if (instrument instanceof SwapInstrument) {
				List<Swap<?, ?>> marketStandardSwaps = new MarketStandardEquivalentSwapBuilder((SwapInstrument) instrument, curve).makeMarketStandardEquivalent();
				Swap<?, ?> derivative = ((SwapInstrument) instrument).getDerivative();
				
				MultipleCurrencyAmount multiCcyUpfront = instrument.getUpfront();
				
				MarketStandardFairFixedRateCalculator MSFFRC = new MarketStandardFairFixedRateCalculator(derivative, multiCcyUpfront, curve, executionTimestamp, marketStandardSwaps);
				double priceMetricTimeSeries = MSFFRC.getFairFixedRate();
				Pair<Double, Double> priceMetricTermStructure = MSFFRC.getTwoPartFairFixedRate();
				
				// calculate delta risk
				DeltaRiskCalculatorOld DRC = new DeltaRiskCalculatorOld(derivative, pair, SCCM.getDeltaDisplayBundle());
				DeltaResults spreadDelta = DRC.extractSpreadDelta();
				DeltaResults fixedRateDelta = DRC.extractFixedDelta();
				
				//logger.info(fixedRateDelta.toString());
				logger.info(ccy+" "+priceMetricTimeSeries+" "+priceMetricTermStructure+" "+taxonomy +" "+ trade.getDissemination_id());

			} else if (instrument instanceof SwaptionInstrument) {
				SwaptionPhysicalFixedIbor[] swaption = (SwaptionPhysicalFixedIbor[]) ((SwaptionInstrument) instrument).getDerivative();
				final PresentValueDiscountingCalculator PVMC = PresentValueDiscountingCalculator.getInstance();
				//logger.info(BlackImpliedVolatilityCalculator.getImpliedVolatility(swaption[0], ((SwaptionInstrument) instrument).getOptionPremium(), curve));
			}
			return 1;	
		}
		catch (Exception e){
			logger.info(trade.getDissemination_id()+" "+trade.getSettlement_currency()+":  "+e.getMessage() );
			e.printStackTrace();
			return -1;
		}
	}


}
