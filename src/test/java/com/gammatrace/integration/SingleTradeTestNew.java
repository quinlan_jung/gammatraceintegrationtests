package com.gammatrace.integration;

import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import com.gammatrace.commonutils.Utils;
import com.gammatrace.datamodel.Trade;
import com.gammatrace.helpers.TestUtils;
import com.gammatrace.logging.CustomLogger;
import com.opengamma.gammatrace.prod.RepriceResult;
import com.opengamma.gammatrace.prod.Repricer;

public class SingleTradeTestNew {
	static {
		Utils.clearDirectory(CustomLogger.getLoggingPath());
	}
	Logger logger = CustomLogger.getLogger(SingleTradeTestNew.class);
	
	@Test
	public void doReprice(){
		TestUtils parserUtils = new TestUtils();
		Set <Trade> trades = parserUtils.getRepriceableTrade();
		// Input the dissemination ID here
		long usdFixedFloat = 17069066;
		long eurFixedFloat = 17095087;
		long gbpFixedFloat = 17077828;
		long jpyFixedFloat = 17069036;
		
		long usdFixedFloatSpread = 17095740;
		
		long usdFixedFloatUpfront = 17095602;
		long eurFixedFloatUpfront = 17098207;
		long gbpFixedFloatUpfront = 17084831;
		long jpyFixedFloatUpfront = 17069288; 
		
		long usdOIS = 17092053;
		long eurOIS = 17084438;
		long gbpOIS = 17084010; 				// looks reasonable
		long jpyOIS = 17070118;
		
		long usdLiborBasis = 17100604;			// this returns an incorrect fair rate because we are not building the compounding coupon properly when index = 3m, but payment freq = 6m.
		long gbpLiborBasis = 17101750;			// REJECTED.. price_notation			 
		long jpyLiborBasis = 17082020;			// fails because we don't have JPYLIBOR3M 
		
		long usdLiborON = 17091082;
		
		long eurUsdXCcyBasisWithoutPriceNotional = 17085165;	// REJECTED...price_notation. We should allow for this (as in IRBasis) iff priceNotional2 is not empty (see updated RepricingCriteria) 
		long eurUsdXCcyBasis = 17100390;		// becomes very slow at some point while requesting the data.. seems related to EUR=ICGC (106 secs total). But does price eventually. 

		long gbpUsdXCcyBasis = 17090889;		// need market std generator.	
		long usdJpyXCcyBasis = 17068456;
		
		long usdJpyXCcyFixedFloat = 17070727;		// failing bc of negative notional. I think this could be because USDJPY (JPY=ICGC) is being returned as -1?
		
		long usdJpyXCcyFixedFixed = 17070381;
		
		long usdSwaption = 17065797;	// 17090023 not in the list because its a corrected trade saved under the original id. Change this.
		long eurSwaption = 17088430;
		long gbpSwaption = 17091734;
		long jpySwaption = 17068381;
		
		long usdSwaptionStraddle = 17081504;
		long eurSwaptionStraddle = 17082017;
		long gbpSwaptionStraddle = 17105238;
		long jpySwaptionStraddle = 17068619;
		
		long temp = 17108735;
		long temp2 = 17107209;
		long temp3 = 17093735;
		
		for (Trade trade : trades) {
			long id = trade.getDissemination_id();
			if (id == temp2) {					// <<<<---------------- INPUT ID HERE
				logger.info("Found search id");
				RepriceResult results = reprice(trade);
				
				logger.info(trade.getPrice_notation());
				logger.info(results.getTradeImpliedFairQuote());
				logger.info(results.getMarketImpliedFairQuote());

			}
		}	
	}
	
	private RepriceResult reprice(Trade trade){
		try{
			return Repricer.reprice(trade);
			
			
		}
		catch (Exception e){
			logger.error(trade.getDissemination_id() +":  "+e.getMessage(), e);
			return null;
		}
	}	
}
