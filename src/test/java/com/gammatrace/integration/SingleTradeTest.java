package com.gammatrace.integration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.gammatrace.commonutils.Utils;
import com.gammatrace.datamodel.Trade;
import com.gammatrace.helpers.TestUtils;
import com.gammatrace.helpers.TradeItem;
import com.gammatrace.logging.CustomLogger;
import com.opengamma.gammatrace.prod.RepriceResult;
import com.opengamma.gammatrace.prod.Repricer;

public class SingleTradeTest {
	static{
		Utils.clearDirectory(CustomLogger.getLoggingPath());
	}
	Logger logger = CustomLogger.getLogger(SingleTradeTest.class);
	private static Map<Long, TradeItem> tradeItems = new HashMap<Long, TradeItem>();
	
	static{
		// ERRORS CONSISTENT WITH COMMIT: b219f6f560fee604599fcbf80233008ba6535348
/*		tradeItems.put(17069066L, new TradeItem (17069066L, "usdFixedFloat"));
		tradeItems.put(17095087L, new TradeItem (17095087L, "eurFixedFloat"));
		tradeItems.put(17077828L, new TradeItem (17077828L, "gbpFixedFloat"));
		tradeItems.put(17069036L, new TradeItem (17069036L, "jpyFixedFloat"));
		
		tradeItems.put(17095740L, new TradeItem (17095740L, "usdFixedFloatSpread"));
		tradeItems.put(17095602L, new TradeItem (17095602L, "usdFixedFloatUpfront"));
		tradeItems.put(17098207L, new TradeItem (17098207L, "eurFixedFloatUpfront"));
		tradeItems.put(17084831L, new TradeItem (17084831L, "gbpFixedFloatUpfront"));
		tradeItems.put(17069288L, new TradeItem (17069288L, "jpyFixedFloatUpfront"));

		tradeItems.put(17092053L, new TradeItem (17092053L, "usdOIS"));
		tradeItems.put(17084438L, new TradeItem (17084438L, "eurOIS"));
		tradeItems.put(17084010L, new TradeItem (17084010L, "gbpOIS")); */
		//tradeItems.put(17069706L, new TradeItem (17069706L, "jpyOIS")); // Rejected Column: indication_of_other_price_affecting_term
		
/*		tradeItems.put(17100604L, new TradeItem (17100604L, "usdLiborBasis")); // TODO: this returns an incorrect fair rate because we are not building the compounding coupon properly when index = 3m, but payment freq = 6m.
		tradeItems.put(17101750L, new TradeItem (17101750L, "gbpLiborBasis"));
		//tradeItems.put(17082020L, new TradeItem (17082020L, "jpyLiborBasis")); //Could not get market standard generator for asset pair:[JPYLIBOR3M, JPYLIBOR6M] (MarketStandardGeneratorMapper.java:110)
		//tradeItems.put(17091082L, new TradeItem (17091082L, "usdLiborON")); // Rejected Column: price_notation_type
		
		tradeItems.put(17100390L, new TradeItem (17100390L, "eurUsdXCcyBasis")); //Rejected Column: price_notation_type
		//tradeItems.put(17085165L, new TradeItem (17085165L, "eurUsdXCcyBasisWithoutPriceNotional")); //filtered out at the moment.. to be fixed
		tradeItems.put(17090889L, new TradeItem (17090889L, "gbpUsdXCcyBasis")); 
		//tradeItems.put(17068456L, new TradeItem (17068456L, "usdJpyXCcyBasis"));  // MultiCurrencyCurveMarketDataProvider JPY Configuration null, line 203
		
		//tradeItems.put(17070727L, new TradeItem (17070727L, "usdJpyXCcyFixedFloat")); // SwapXCcyFixedIborBuilder Compilation
		//tradeItems.put(17070381L, new TradeItem (17070381L, "usdJpyXCcyFixedFixed")); // Could not get index for FIXED JPY null (AssetMapper.java:117)
		
		tradeItems.put(17065797L, new TradeItem (17065797L, "usdSwaption"));
		tradeItems.put(17088430L, new TradeItem (17088430L, "eurSwaption"));  // Casting at com.opengamma.gammatrace.prod.Repricer.reprice(Repricer.java:65)
		//tradeItems.put(17091734L, new TradeItem (17091734L, "gbpSwaption")); // SABRMarketDataProvider No GBP configuration
		//tradeItems.put(17068381L, new TradeItem (17068381L, "jpySwaption")); // SABRMarketDataProvider No JPY configuration
		
		tradeItems.put(17081504L, new TradeItem (17081504L, "usdSwaptionStraddle"));
		//tradeItems.put(17082017L, new TradeItem (17082017L, "eurSwaptionStraddle")); // Failing bc the execution timestamp is far back and weird, so it can't find data, returns -1 and can't build curve/vol
		tradeItems.put(17105021L, new TradeItem (17105021L, "gbpSwaptionStraddle")); // SABRMarketDataProvider No GBP configuration
		tradeItems.put(17068619L, new TradeItem (17068619L, "jpySwaptionStraddle")); // SABRMarketDataProvider No JPY configuration
		
		tradeItems.put(17081504L, new TradeItem (17081504L, "usdSwaptionStraddle"));
		//tradeItems.put(17082017L, new TradeItem (17082017L, "eurSwaptionStraddle")); // Implied volatility issue http://pastebin.com/JBRqKa2F
		tradeItems.put(17105021L, new TradeItem (17105021L, "gbpSwaptionStraddle")); // SABRMarketDataProvider No GBP configuration
		tradeItems.put(17068619L, new TradeItem (17068619L, "jpySwaptionStraddle")); // SABRMarketDataProvider No JPY configuration
*/		
		tradeItems.put(17106338L, new TradeItem (17106338L, "Xccy period 28D error"));

		// OIS Basis error: Could not get index for code: USD-LIBOR-BBA, currency: USD, period: P28D
		// Forward curve not found: JPYLIBOR3M MulticurveProviderDiscount.getForwardRate(MulticurveProviderDiscount.java:234)
	}
	
	@Test
	public void doReprice(){
		TestUtils parserUtils = new TestUtils ();
		Set <Trade> trades = parserUtils.getRepriceableTrade();
		List<Long> orderOfReprice = new ArrayList<Long> ();
		List<RepriceResult> repriceResults = new ArrayList<RepriceResult> ();
		for (Trade trade : trades) {
			long id = trade.getDissemination_id();
			if (tradeItems.containsKey(id)){
				orderOfReprice.add(id);
				TradeItem tradeItem = tradeItems.get(id);
				logger.info(String.format("Repricing dissemination id : %d", id));
				tradeItem.startTimer();
				RepriceResult repriceResult = reprice(trade);
				if (repriceResult != null){
					repriceResults.add(repriceResult);
				} 
			}
		}
	
		for (Long id : orderOfReprice){
			TradeItem tradeItem = tradeItems.get(id);
			tradeItem.printResults();
		}
		
		Assert.assertEquals(tradeItems.size(), repriceResults.size());
	}
	
	private RepriceResult reprice(Trade trade){
		try{
			return Repricer.reprice(trade);	
		}
		catch (Exception e){
			logger.error(trade.getSettlement_currency() +":  "+e.getMessage() + trade.getDissemination_id(), e);
			return null;
		}
	}
	
	
}